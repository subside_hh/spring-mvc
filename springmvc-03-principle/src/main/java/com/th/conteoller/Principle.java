package com.th.conteoller;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author ThreePure
 * @date 2022/3/13 23:00
 * @description: TODO
 * @since 1.8
 */
public class Principle implements Controller {

    @Override
    public ModelAndView handleRequest(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws Exception {
        ModelAndView view = new ModelAndView();

        //业务代码
        String result = "HelloSpringMVC";
        view.addObject("msg", result);

        //视图跳转
        view.setViewName("test");

        return view;
    }
}
