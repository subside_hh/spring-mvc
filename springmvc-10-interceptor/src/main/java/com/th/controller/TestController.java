package com.th.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author ThreePure
 * @date 2022/3/25 15:32
 * @description:  控制器
 * @since 1.8
 */
@Controller
public class TestController {

    @RequestMapping("/testInterceptor")
    public String testInterceptor(){
        return "success";
    }

    @RequestMapping("/testExceptionHandle")
    public String testExceptionHandle(){
        System.out.println(1/0);
        return "success";
    }
}
