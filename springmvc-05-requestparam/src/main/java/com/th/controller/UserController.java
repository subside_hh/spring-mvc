package com.th.controller;

import com.th.pojo.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author ThreePure
 * @date 2022/3/19 15:18
 * @description: TODO
 * @since 1.8
 */
@Controller
public class UserController {

    /**http://localhost:8080/t1?name=3P*/
    @GetMapping("/t1")
    public String test1(String name, Model model){
        //1、接收前端参数
        System.out.println("接收到前端数据为："+name);
        //2、将返回结果传递给前端 Model
        model.addAttribute("msg", name);
        //3、视图跳转
        return "test";
    }

    /**http://localhost:8080/t2?username=ThreePure
     * 通过@RequestParam注解完成前端参数名与接收端参数的映射*/
    @GetMapping("/t2")
    public String test2(@RequestParam("username")String name, Model model){
        //1、接收前端参数
        System.out.println("接收到前端数据为："+name);
        //2、将返回结果传递给前端 Model
        model.addAttribute("msg", name);
        //3、视图跳转
        return "test";
    }

    /**接收一个对象
     * http://localhost:8080/t3?id=2&name=ThreePure&age=23
     * 注意：传递的参数名一定要跟对象中属性名一致，如果不一致则为null */
    @GetMapping("/t3")
    public String test3(User user, Model model){
        //1、接收前端参数
        System.out.println("接收到前端数据为："+user);
        //2、将返回结果传递给前端 Model
        model.addAttribute("msg", user);
        //3、视图跳转
        return "test";
    }


}
