package com.th.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author ThreePure
 * @date 2022/3/19 19:39
 * @description: TODO
 * @since 1.8
 */
@Controller
public class EncodingController {
    @RequestMapping("/e/t")
    public String test(Model model, String name){
        System.out.println(name);
        model.addAttribute("msg",name); //获取表单提交的值
        return "test"; //跳转到test页面显示输入的值
    }
}
