package com.th.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author ThreePure
 * @date 2022/3/20 17:57
 * @description:  多个控制器方法对应一个请求报错案例
 *
 * @since 1.8
 */
@Controller
public class TwoController {
    /**【报错】
     * com.th.controller.TwoController#twoContr()
     * to { [/]}: There is already 'helloController' bean method
     * com.th.controller.HelloController#hello() mapped.*/
    //@RequestMapping("/")
    public String twoContr(){
        return "target";
    }
}
