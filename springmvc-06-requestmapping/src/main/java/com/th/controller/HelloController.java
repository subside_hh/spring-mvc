package com.th.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * @author ThreePure
 * @date 2022/3/20 17:47
 * @description: TODO
 * @since 1.8
 */
@Controller
public class HelloController {

    @RequestMapping(
            value = {"/", "/hello"},
            method = {RequestMethod.POST, RequestMethod.GET}
    )
    public String hello(){
        return "index";
    }

    @RequestMapping(
            value = { "/test"},
            method = {RequestMethod.POST, RequestMethod.GET},
            params = {"username","password!=123"},
            headers = {"Host=localhost:8080"}
    )
    public String paramAnd(){
        return "sucess";
    }

    @RequestMapping("ant1/a?a")
    public String antTest1(){
        return "sucess";
    }

    @RequestMapping("ant2/a*b")
    public String antTest2(){
        return "sucess";
    }

    @RequestMapping("**/ant3")
    public String antTest3(){
        return "sucess";
    }

    @RequestMapping("/testRest/{id}/{username}")
    public String testRest(@PathVariable("id") String id, @PathVariable("username") String username){
        System.out.println("==========>>>id:"+id+",username:"+username);
        return "sucess";
    }
//最终输出的内容为-->id:1,username:admin
}
