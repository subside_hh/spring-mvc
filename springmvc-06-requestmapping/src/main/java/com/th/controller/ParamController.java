package com.th.controller;

import com.th.pojo.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

/**
 * @author ThreePure
 * @date 2022/3/20 23:08
 * @description: 请求参数相干控制器方法
 * @since 1.8
 */
@Controller
public class ParamController {

    @RequestMapping("/paramServletAPI")
    public String paramTest1(HttpServletRequest request){
        String username = request.getParameter("username");
        System.out.println("username:" + username);
        String password = request.getParameter("password");
        System.out.println("password:" + password);
        return "test_param";
    }

    @RequestMapping("/paramHeader")
    public String paramTest2(@RequestHeader(value = "Host",required = true,defaultValue = "host8080") String hostName){
        System.out.println("hostName:"+hostName);
        return "sucess";
    }

    @RequestMapping("/paramCookie")
    public String paramTest3(@CookieValue(value = "created",required = true,defaultValue = "aaa")String cookie){
        System.out.println("Cookie:"+cookie);
        return "sucess";
    }

    @RequestMapping("/testpojo")
    public String testPOJO(User user){
        System.out.println(user);
        return "sucess";
    }
}
