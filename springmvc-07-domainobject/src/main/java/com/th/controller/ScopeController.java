package com.th.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Map;

/**
 * @author ThreePure
 * @date 2022/3/21 22:11
 * @description: TODO
 * @since 1.8
 */
@Controller
public class ScopeController {

    @RequestMapping("/testRequestByServletAPI")
    public String testRequestByServletAPI(HttpServletRequest request){
        request.setAttribute("testRequestScope", "Hello,servletAPI");
        return "success";
    }

    @RequestMapping("/testRequestByModelAndView")
    public ModelAndView testRequestByModelAndView(){
        ModelAndView modelAndView = new ModelAndView();
        //向请求域共享数据
        modelAndView.addObject("testRequestScope", "Hello,ModelAndView");
        //设置视图，实现页面跳转
        modelAndView.setViewName("success");
        return modelAndView;
    }

    @RequestMapping("/testRequestByModel")
    public String testRequestByModel(Model model){
        model.addAttribute("testRequestScope","Hello,Model");
        System.out.println(model.getClass().getName());
        return "success";
    }

    @RequestMapping("/testRequestByMap")
    public String testRequestByMap(Map<String, Object> map){
        map.put("testRequestScope","Hello,Map");
        System.out.println(map.getClass().getName());
        return "success";
    }

    @RequestMapping("/testRequestByModelMap")
    public String testRequestByModelMap(ModelMap modelMap){
        modelMap.addAttribute("testRequestScope","Hello,ModelMap");
        System.out.println(modelMap.getClass().getName());
        return "success";
    }

    @RequestMapping("/testSessionByServletAPI")
    public String testSessionByServletAPI(HttpSession session){
        session.setAttribute("testSessionScope", "hello,session");
        return "success";
    }

    @RequestMapping("/testApplicationByServletAPI")
    public String testApplicationByServletAPI(HttpSession session){
        ServletContext servletContext = session.getServletContext();
        servletContext.setAttribute("testApplicationScope", "hello,application");
        return "success";
    }

}
