package com.th.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author ThreePure
 * @date 2022/3/21 22:03
 * @description: TODO
 * @since 1.8
 */
@Controller
public class TestController {

    /*@RequestMapping("/")
    public String test1(){
        return "index";
    }
*/
    @RequestMapping("/test_view")
    public String testView(){
        return "test_view";
    }
}
