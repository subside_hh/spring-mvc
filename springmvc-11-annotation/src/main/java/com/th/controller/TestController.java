package com.th.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author ThreePure
 * @date 2022/3/25 18:03
 * @description: TODO
 * @since 1.8
 */
@Controller
public class TestController {
    @RequestMapping("/")
    public String index(){
        return "index";
    }

    @RequestMapping("/error")
    public String error(){
        System.out.println(1/0);
        return "index";
    }
}
