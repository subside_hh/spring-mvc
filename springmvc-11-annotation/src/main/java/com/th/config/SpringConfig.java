package com.th.config;

import org.springframework.context.annotation.Configuration;

/**
 * @author ThreePure
 * @date 2022/3/25 17:35
 * @description: Spring配置类,
 * @since 1.8
 */
@Configuration
public class SpringConfig {
}
