package com.th.config;

import org.springframework.web.filter.CharacterEncodingFilter;

import org.springframework.web.filter.HiddenHttpMethodFilter;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

import javax.servlet.Filter;

/**
 * @author ThreePure
 * @date 2022/3/25 17:30
 * @description: 注解配置SpringMVC初始化类,代替web.xml
 * @since 1.8
 */
public class WebInit extends AbstractAnnotationConfigDispatcherServletInitializer {

    /**
     * @description:  指定spring的配置类
     * @date 2022/3/25 17:33
     * @Param: []
     * @Return: java.lang.Class<?>[]
     */
    @Override
    protected Class<?>[] getRootConfigClasses() {
        return new Class[]{SpringConfig.class};
    }

    /**
     * @description:  指定SpringMVC的配置类
     * @date 2022/3/25 17:34
     * @Param: []
     * @Return: java.lang.Class<?>[]
     */
    @Override
    protected Class<?>[] getServletConfigClasses() {
        
        return new Class[]{WebConfig.class};
    }

    /**
     * @description:  指定DispatcherServlet的映射规则，即url-pattern
     * @date 2022/3/25 17:34
     * @Param: []
     * @Return: java.lang.String[]
     */
    @Override
    protected String[] getServletMappings() {
        return new String[]{"/"};
    }

    /**
     * @description:  添加过滤器
     * @date 2022/3/25 17:41
     * @Param: []
     * @Return: javax.servlet.Filter[]
     */
    @Override
    protected Filter[] getServletFilters() {
        //编码过滤器
        CharacterEncodingFilter encodingFilter = new CharacterEncodingFilter();
        encodingFilter.setEncoding("UTF-8");
        encodingFilter.setForceRequestEncoding(true);
        //put、delete方法支持[HiddenHttpMethodFilter]

        HiddenHttpMethodFilter hiddenHttpMethodFilter = new HiddenHttpMethodFilter();
        return new Filter[]{encodingFilter, hiddenHttpMethodFilter};
    }
}
