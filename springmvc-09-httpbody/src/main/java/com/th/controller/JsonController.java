package com.th.controller;

import com.th.pojo.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author ThreePure
 * @date 2022/3/24 19:46
 * @description: TODO
 * @since 1.8
 */
@Controller
public class JsonController {

    @RequestMapping("/testResponseBodyUser")
    @ResponseBody
    public User testResponseBodyUser(){
        return new User(1,"admin","pw123","男","admain@163.com");
    }

    @RequestMapping("/testAjax")
    @ResponseBody
    public String testAjax(String username, String password){
        System.out.println("username:"+username+",password:"+password);
        return "hello,ajax";
    }
}
