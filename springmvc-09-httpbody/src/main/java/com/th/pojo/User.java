package com.th.pojo;

/**
 * @author ThreePure
 * @date 2022/3/24 19:44
 * @description: TODO
 * @since 1.8
 */
public class User {
    private Integer id;
    private String username;
    private String password;
    private String gender;
    private String email;

    public User() {
    }

    public User(Integer id, String username, String password, String gender, String email) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.gender = gender;
        this.email = email;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
