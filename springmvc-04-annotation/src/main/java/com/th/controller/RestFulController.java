package com.th.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

/**
 * @author ThreePure
 * @date 2022/3/19 11:54
 * @description: RestFul风格演示
 * @since 1.8
 */
@Controller
public class RestFulController {

    /**传统方法
     * http://localhost:8080/add?a=1&b=2
     * */
    @RequestMapping("/add")
    public String test1(int a,int b, Model model){
        int res = a + b;
        model.addAttribute("msg", "【1】参数结果为"+res);
        return "hello";
    }

    /**RestFul风格
     * http://localhost:8080/add2/1/2 */
    @RequestMapping("/add2/{a}/{b}")
    public String test2(@PathVariable int a, @PathVariable int b, Model model){
        int res = a + b;
        model.addAttribute("msg", "【2】参数结果为"+res);
        return "hello";
    }

    /**Get方式提交*/
    @RequestMapping(value = "/add3/{a}/{b}",method = RequestMethod.GET)
    public String test3(@PathVariable int a, @PathVariable int b, Model model){
        int res = a + b;
        model.addAttribute("msg", "【3】参数结果为"+res);
        return "hello";
    }

    /**Get方式提交[简写]*/
    @GetMapping("/add/{a}/{b}")
    public String test4(@PathVariable int a, @PathVariable int b, Model model){
        int res = a + b;
        model.addAttribute("msg", "【4】参数结果为"+res);
        return "hello";
    }

    /**Post方式提交*/
    @PostMapping("/add/{a}/{b}")
    public String test5(@PathVariable int a, @PathVariable int b, Model model){
        int res = a + b;
        model.addAttribute("msg", "【5】参数结果为"+res);
        return "hello";
    }

}
