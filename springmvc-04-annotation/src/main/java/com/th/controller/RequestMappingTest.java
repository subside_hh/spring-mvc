package com.th.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author ThreePure
 * @date 2022/3/19 11:29
 * @description: TODO
 * @since 1.8
 */
@Controller
@RequestMapping("/r")
public class RequestMappingTest {

    @RequestMapping("/mapping")
    public String requestMapping1(Model model) {
        model.addAttribute("msg", "RequestMapping1");
        return "hello";
    }

}
